
const vid = document.getElementById("vid");
const timer = document.getElementById("timer");
const start = document.getElementById("start");

let is24 = false;
if (window.location.search === "?24h") {
	vid.src = "background_24h.mp4";
	is24 = true;
}

if (window.location.search === "?50") {
	vid.src = "background_50.mp4";
}


const onclick = () => { 
    if (vid.paused) vid.play();
}

function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

vid.onloadeddata = () => {
    const ratioY = parseInt(window.getComputedStyle(vid).height.replace("px", "")) / 1230;
    const ratioX = parseInt(window.getComputedStyle(vid).width.replace("px", "")) / 720;
    
    timer.style.top = `${Math.round(475 * ratioY)}px`;
    timer.style.left = `${Math.round(332 * ratioX) + 1}px`;
    timer.style.transform = `scale(${ratioX})`;

    start.style.top = `${Math.round((is24 ? 858 : 868) * ratioY)}px`;
    start.style.left = `${Math.round(591 * ratioX)}px`;
    start.style.transform = `scale(${ratioX})`;

    document.body.style.visibility = "visible";
}


document.addEventListener("mousedown", onclick);
document.addEventListener("touchstart", onclick);

let initialMin = min = randomIntFromInterval(39, 49);
let sec = (new Date()).getSeconds();

function casefy(num) {
    if (num > 9) return `${num}`;
    else return "0" + num;
}
function updateTimer() {
    const current = new Date();
    let dur = (current.getTime() - sDate.getTime())/1000;
    dur /= 60;
    dur = Math.abs(Math.round(dur));
    reel = 60 - dur;
    if (Math.abs(min - reel) > 1) {
        min = reel;
    }

    timer.innerText = `${casefy(min)}:${casefy(sec)}`;
    sec--;
    if (sec < 0) { 
        sec = 59;
        min--;
        if (min < 0) {
            min = initialMin;
            updateStart();
        }
    }
}

let sDate;
function updateStart() {
    const current = new Date();
    sDate = current;
    let shour = current.getHours();
    let smin = current.getMinutes();

    smin = smin - 60 + min;
    if (smin < 0) {
        smin = 60 - Math.abs(smin);
        shour--;
        
        if (shour < 0) {
            shour = 24 - Math.abs(shour);
        }
    }
    sDate.setHours(shour);
    sDate.setMinutes(smin);
    sDate.setSeconds(sec);
    sDate.setMilliseconds(0);
    start.innerText = `${casefy(shour)}h${casefy(smin)}`;
}
updateStart();

setInterval(() => updateTimer(), 1000);
updateTimer();